﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _26._08_Targil
{
    class Program
    {
        static void Main(string[] args)
        {
            SelfImplementStack myStack = new SelfImplementStack();
            myStack.Push(1);  // [1]
            myStack.Push(3);  // [3, 1]
            myStack.Push(-4); // [-4, 3, 1]
            int z1 = myStack.Pop();  // z1 = -4. [3, 1]
            int z2 = myStack.Peek(); // z2 =  3. [3, 1]
            myStack.Pop(); // [1]
            myStack.Pop(); // []
            myStack.Pop(); // []
        }
    }
}
