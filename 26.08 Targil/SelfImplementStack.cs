﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _26._08_Targil
{
    class SelfImplementStack
    {
        private List<int> _items;
        // pop -- remove the first item from the list -- index 0

        public SelfImplementStack()
        {
            _items = new List<int>();

        }

        public int Pop()
        {
            if (_items.Count == 0)
                return 0;
            int result = _items[0];
            _items.RemoveAt(0);
            return result;
        }
        public void Push(int x)
        {
            _items.Insert(0,x);
        }
        public int Peek()
        {
            return _items[0];
        }
        public void Clear()
        {
            _items.Clear();
        }
    }
}

